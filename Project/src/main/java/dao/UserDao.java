package dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import model.User;

public class UserDao {
  /**
   * ログインIDとパスワードに紐づくユーザ情報を返す S
   * 
   * @param loginId
   * @param password
   * @return
   */
  public User findByLoginInfo(String loginId, String password) {
    Connection conn = null;
    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      // SELECT文を準備
      String sql = "SELECT * FROM user WHERE login_id = ? and password = ?";

      // SELECTを実行し、結果表を取得
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, loginId);
      pStmt.setString(2, password);
      ResultSet rs = pStmt.executeQuery();

      // 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
      if (!rs.next()) {
        return null;
      }

      // 必要なデータのみインスタンスのフィールドに追加
      String loginIdData = rs.getString("login_id");
      String nameData = rs.getString("name");
      return new User(loginIdData, nameData);

    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
  }

  public boolean findByLoginIdInfo(String loginId) {
    Connection conn = null;
    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      // SELECT文を準備
      String sql = "SELECT * FROM user WHERE login_id = ?";

      // SELECTを実行し、結果表を取得
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, loginId);
      ResultSet rs = pStmt.executeQuery();

      // 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
      if (!rs.next()) {

      }


    } catch (SQLException e) {
      e.printStackTrace();

    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();

        }
      }
    }
    return false;
  }
  public void userAdd(String loginId, String name, String birthDate, String password) {
    Connection conn = null;
    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      // INSERT文を準備
      String sql =
          "INSERT INTO user login_id , name , birth_Date , password , create_date , update_date) VALUES (? , ? , ? ,? , now() , now())";

      // INSERTを実行し、結果表を取得
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, loginId);
      pStmt.setString(2, name);
      pStmt.setString(3, birthDate);
      pStmt.setString(4, password);
      pStmt.executeUpdate();

    } catch (SQLException e) {
      e.printStackTrace();
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }
  }

  public User userDetail(int id) {
    Connection conn = null;
    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      // SELECT文を準備
      String sql =
          "SELECT * FROM user WHERE id = ?";

      // SELECTを実行し、結果表を取得
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setInt(1, id);

      ResultSet rs = pStmt.executeQuery();

      // 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
      if (!rs.next()) {
        return null;
      }

      // 必要なデータのみインスタンスのフィールドに追加
      String loginIdData = rs.getString("login_id");
      String nameData = rs.getString("name");
      Date birthDateData = rs.getDate("birth_date");
      Timestamp createDateData = rs.getTimestamp("create_date");
      Timestamp updateDateData = rs.getTimestamp("update_date");
      return new User(loginIdData, nameData, birthDateData, createDateData, updateDateData);

    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
  }

  public User findById(int id) {
    Connection conn = null;
    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      // SELECT文を準備
      String sql = "SELECT login_id , name , birth_date  FROM user WHERE id = ?";

      // SELECTを実行し、結果表を取得
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setInt(1, id);
      ResultSet rs = pStmt.executeQuery();

      // 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
      if (!rs.next()) {
        return null;
      }

      // 必要なデータのみインスタンスのフィールドに追加
      String loginIdData = rs.getString("login_id");
      String nameData = rs.getString("name");
      Date birthDateData = rs.getDate("birth_date");
      return new User(id, loginIdData, nameData, birthDateData);

    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
  }

  public void userexpassUpdate(int id, String name, String birthDate) {
    Connection conn = null;
    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      // UPDATE文を準備
      String sql = "UPDATE user SET name = ? , birth_date = ? , update_date = now()  WHERE id = ?";

      // UPDATEを実行
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, name);
      pStmt.setString(2, birthDate);
      pStmt.setInt(3, id);
      pStmt.executeUpdate();

    } catch (SQLException e) {
      e.printStackTrace();
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }
  }
  public void userUpdate(int id, String name, String birthDate, String password) {
    Connection conn = null;
    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      // UPDATE文を準備
      String sql =
          "UPDATE user SET name = ? , birth_date = ? , password = ? , update_date = now()  WHERE id = ?";

      // UPDATEを実行
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, name);
      pStmt.setString(2, birthDate);
      pStmt.setString(3, password);
      pStmt.setInt(4, id);
      pStmt.executeUpdate();

    } catch (SQLException e) {
      e.printStackTrace();
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }
  }

  public User userDeleteInfo(int id) {
    Connection conn = null;
    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      // SELECT文を準備
      String sql = "SELECT login_id  FROM user WHERE id = ?";

      // SELECTを実行し、結果表を取得
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setInt(1, id);
      ResultSet rs = pStmt.executeQuery();

      // 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
      if (!rs.next()) {
        return null;
      }

      // 必要なデータのみインスタンスのフィールドに追加
      String loginIdData = rs.getString("login_id");
      return new User(id, loginIdData);

    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
  }

  public void userDelete(int id) {
    Connection conn = null;
    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      // Delete文を準備
      String sql = "DELETE FROM user WHERE id = ?";

      // INSERTを実行し、結果表を取得
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setInt(1, id);
      pStmt.executeUpdate();

    } catch (SQLException e) {
      e.printStackTrace();
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }
  }
  /**
   * 全てのユーザ情報を取得する
   *
   * @return
   */
  public List<User> findAll() {
    Connection conn = null;
    List<User> userList = new ArrayList<User>();

    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      // SELECT文を準備
      // TODO: 未実装：管理者以外を取得するようSQLを変更する
      String sql = "SELECT * FROM user WHERE is_admin = false";

      // SELECTを実行し、結果表を取得
      Statement stmt = conn.createStatement();
      ResultSet rs = stmt.executeQuery(sql);

      // 結果表に格納されたレコードの内容を
      // Userインスタンスに設定し、ArrayListインスタンスに追加
      while (rs.next()) {
        int id = rs.getInt("id");
        String loginId = rs.getString("login_id");
        String name = rs.getString("name");
        Date birthDate = rs.getDate("birth_date");
        String password = rs.getString("password");
        boolean isAdmin = rs.getBoolean("is_admin");
        Timestamp createDate = rs.getTimestamp("create_date");
        Timestamp updateDate = rs.getTimestamp("update_date");
        User user =
            new User(id, loginId, name, birthDate, password, isAdmin, createDate, updateDate);

        userList.add(user);
      }
    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
    return userList;
  }
}

