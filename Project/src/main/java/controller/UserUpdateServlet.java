package controller;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserUpdateServlet
 */
@WebServlet("/UserUpdateServlet")
public class UserUpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserUpdateServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      request.setCharacterEncoding("UTF-8");

      // リクエストパラメータの入力項目を取得
      int id = Integer.parseInt(request.getParameter("id"));


      // 確認用：idをコンソールに出力
      System.out.println(id);


      // ユーザー情報（ログインId、ユーザー名、生年月日）を取得
      UserDao userDao = new UserDao();
      User user = userDao.findById(id);

      // リクエストスコープにユーザー情報をセット
      request.setAttribute("userIdInfo", user);
      
      //ユーザー更新のjspにフォワード
      RequestDispatcher dispatcher =
          request.getRequestDispatcher("/WEB-INF/jsp/MockSample/userUpdate.jsp");
      dispatcher.forward(request, response);
	}



	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      // リクエストパラメータの文字コードを指定
      request.setCharacterEncoding("UTF-8");

      // リクエストパラメータの入力項目を取得
      int id = Integer.parseInt(request.getParameter("user-id"));
       String name = request.getParameter("user-name");
      String birthDate = request.getParameter("birth-date");
      String password = request.getParameter("password");
      
      
      // パスワードを更新しないとき、userexpassUpdateメソッドを呼び出す
      if(password.equals("")){
      UserDao userDao = new UserDao();
      userDao.userexpassUpdate(id, name, birthDate);
      
      // パスワードを更新するとき、userUpdateメソッドを呼び出す
    } else {
      UserDao userDao = new UserDao();
      userDao.userUpdate(id, name, birthDate, password);
      }
      
      // UserListサーブレットへ遷移
      response.sendRedirect("UserListServlet");


    }

}