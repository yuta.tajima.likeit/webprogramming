package controller;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import dao.UserDao;

/**
 * Servlet implementation class UserAddServlet
 */
@WebServlet("/UserAddServlet")
public class UserAddServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserAddServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

      // フォワード
      RequestDispatcher dispatcher =
          request.getRequestDispatcher("/WEB-INF/jsp/MockSample/userAdd.jsp");
      dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      // リクエストパラメータの文字コードを指定
      request.setCharacterEncoding("UTF-8");

      // リクエストパラメータの入力項目を取得
      String loginId = request.getParameter("user-loginid");
      String name = request.getParameter("user-name");
      String birthDate = request.getParameter("birth-date");
      String password = request.getParameter("password");
      String confirmpassword = request.getParameter("password-confirm");
      System.out.println(name);
      UserDao userDao = new UserDao();
      //以下の場合、登録失敗としてデータの登録は行わない。
      //①すでに登録されているログインIDが入力された場合
      if (!userDao.findByLoginIdInfo(loginId)) {

        // リクエストスコープにエラーメッセージをセット
        request.setAttribute("errMsg", "入力された内容は正しくありません。");
        System.out.println("あ");
        // 入力したログインIDを画面に表示するために値をセット
        request.setAttribute("loginId", loginId);
        request.setAttribute("name", name);
        request.setAttribute("birthdate", birthDate);

        // userAddjspにフォワード
        RequestDispatcher dispatcher =
            request.getRequestDispatcher("/WEB-INF/jsp/MockSample/userAdd.jsp");
        dispatcher.forward(request, response);
        return;
      }
      //②パスワードとパスワード（確認）の入力内容が異なる場合
      if (password != confirmpassword) {
        // リクエストスコープにエラーメッセージをセット
        request.setAttribute("errMsg", "入力された内容は正しくありません。");

        // 入力したログインIDを画面に表示するために値をセット
        request.setAttribute("loginId", loginId);
        request.setAttribute("username", name);
        request.setAttribute("birthdate", birthDate);

        // userAddjspにフォワード
        RequestDispatcher dispatcher =
            request.getRequestDispatcher("/WEB-INF/jsp/MockSample/userAdd.jsp");
        dispatcher.forward(request, response);
        return;
      }
      //③入力項目に１つでも未入力のものがある場合
      if (loginId.equals("") || name.equals("") || birthDate.equals("") || password.equals("")
          || confirmpassword.equals("")) {
        // リクエストスコープにエラーメッセージをセット
        request.setAttribute("errMsg", "入力された内容は正しくありません。");

        // 入力したログインIDを画面に表示するために値をセット
        request.setAttribute("loginId", loginId);
        request.setAttribute("username", name);
        request.setAttribute("birthdate", birthDate);

        // userAddjspにフォワード
        RequestDispatcher dispatcher =
            request.getRequestDispatcher("/WEB-INF/jsp/MockSample/userAdd.jsp");
        dispatcher.forward(request, response);
        return;
      }

      // リクエストパラメータの入力項目を引数に渡して、userAddメソッドを呼び出す
      userDao.userAdd(loginId, name, birthDate, password);


      // UserListサーブレットへ遷移
      response.sendRedirect("UserListServlet");


	}

}
