package controller;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserDeleteServlet
 */
@WebServlet("/UserDeleteServlet")
public class UserDeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserDeleteServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      // URLからGETパラメータとしてIDを受け取る
      int id = Integer.parseInt(request.getParameter("id"));

      // 確認用：idをコンソールに出力
      System.out.println(id);

      // TODO 未実装：idを引数にして、idに紐づくユーザ情報を出力する

      UserDao userDao = new UserDao();
      User user = userDao.userDeleteInfo(id);
      System.out.println(user.getId() + "あ");
      // TODO 未実装：ユーザ情報をリクエストスコープにセットしてjspにフォワード
      request.setAttribute("useridInfo", user);
      RequestDispatcher dispatcher =
          request.getRequestDispatcher("/WEB-INF/jsp/MockSample/userDelete.jsp");
      dispatcher.forward(request, response);
    }



	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      // リクエストパラメータの文字コードを指定
      request.setCharacterEncoding("UTF-8");

      // リクエストパラメータの入力項目を取得
      int id = Integer.parseInt(request.getParameter("user-id"));

      // userDeleteメソッドを呼び出す
        UserDao userDao = new UserDao();
      userDao.userDelete(id);

      // UserListサーブレットへ遷移
      response.sendRedirect("UserListServlet");


    }

}